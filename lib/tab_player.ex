defmodule TabPlayer do
  def parse(tab) do
    String.split(tab, "\n")
    |> Enum.filter(fn x -> x != "" end)
    |> Enum.map(fn line -> parse_line(line) end)
    |> Enum.filter(fn x -> x != [] end)
    |> List.flatten
    |> Enum.sort()
    |> Enum.map(fn t -> Tuple.to_list(t) end) 
    |> Enum.map(fn lista -> List.delete_at(lista, 0) end)  
    |> List.flatten 
    |> Enum.join (" ")   
    |> IO.inspect
  end

  def parse_line(line) do
      letter = String.first(line)
      String.split(line, "", trim: true)
      |> Enum.filter(fn x -> x != letter end)
      |> Enum.filter(fn x -> x != "|" end)
      |> Enum.map(fn x -> letter <> x end)
      |> Enum.with_index()
      |> Enum.filter(fn {x,_} -> x != letter <> "-" end)
      |> Enum.map(fn {a,b} -> {b,a} end)
       
      
  end

end
